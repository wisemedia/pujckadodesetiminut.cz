<?php
/*
Plugin Name: Related Services
Description: Configura y muestra servicios relacionados
Version: 0.0.1
Author: SEOCOM
Author URI: http://www.seocom.es/
*/
// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

function servicios_relacionados_links($links) {
	$extra_links = array(
		'<a href="'.admin_url('options-general.php?page=related-services').'">'.__('Settings').'</a>',
	);
	return array_merge($extra_links, $links);
}

require_once dirname(__FILE__).'/config.php';
require_once dirname(__FILE__).'/widget.class.php';

if ( is_admin() ) {
	require_once(SERVICIOS_RELACIONADOS_PLUGIN_DIR . '/options.php');
	add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'servicios_relacionados_links');
}

add_action('widgets_init', function() {
	register_widget('RelatedServicesWidget');
});