<?php
/*
Plugin Name: Calculator Tool
Description: Calculadora créditos online
Version: 0.0.1
Author: SEOCOM
Author URI: http://www.seocom.es/
*/
// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

require_once dirname(__FILE__).'/config.php';
require_once dirname(__FILE__).'/widget.class.php';

add_action('widgets_init', function() {
	register_widget('CalculatorToolWidget');
	add_shortcode('calculator_tool', function(){
		return CalculatorToolWidget::calculator();
	});
});