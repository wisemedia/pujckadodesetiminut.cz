<?php get_header();

$searchCriteria = array();
if (!empty($_GET['s'])) {
	$searchCriteria[] = sprintf('<em>“%s”</em>', $_GET['s']);
}
if (!empty($_GET['date-from']) || !empty($_GET['date-to'])) {
	if (empty($_GET['date-to'])) {
		$searchCriteria[] = sprintf('posteriores al <em>%s</em>', date_i18n(get_option('date_format'), strtotime($_GET['date-from'])));
	}
	elseif (empty($_GET['date-from'])) {
		$searchCriteria[] = sprintf('anteriores al <em>%s</em>', date_i18n(get_option('date_format'), strtotime($_GET['date-to'])));
	}
	else {
		$searchCriteria[] = sprintf('entre el <em>%s</em> y el <em>%s</em>', date_i18n(get_option('date_format'), strtotime($_GET['date-from'])), date_i18n(get_option('date_format'), strtotime($_GET['date-to'])));
	}
}
if (!empty($_GET['cat'])) {
	$categories = array();
	foreach ($_GET['cat'] as $category_id) {
		$categories[] = sprintf('<a href="%s"><em>%s</em></a>', get_category_link($category_id), get_category($category_id)->name);
	}
	if (!empty($searchCriteria)) {
		$searchCriteria[] = sprintf('en las categorias: %s', implode(', ', $categories));
	}
	else {
		$searchCriteria[] = implode(', ', $categories);
	}
}
?>
	<div class="container-fluid main-container">
		<div class="row">
			<div class="col-xs-12">
				<h1><?php _e('Search'); ?></h1>
				<div class="search-info">
					<div class="row">
						<div class="col-xs-12">
							<?php
							if (empty($searchCriteria)) {
								$searchCriteria[] = sprintf('<i>%s</i>', __('todas las categorías, cualquier fecha', 'holaluz'));
							}
							_e('Buscando por los siguientes criterios: ', 'holaluz'); ?><?php echo implode(', ', $searchCriteria); ?>.
							<?php if (!empty($_GET['order'])): ?>
								<br />
								<?php _e('Ordenando los resultados por número de votos.', 'holaluz'); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<main role="main" class="col-xs-12 col-md-9 col-lg-9">
				<div class="posts row">
					<?php get_template_part('loop'); ?>
				</div>
				<?php get_template_part('pagination'); ?>
			</main>
			<div class="hidden-xs hidden-sm col-md-3 col-lg-3">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>