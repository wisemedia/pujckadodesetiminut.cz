<?php get_header(); ?>
	<div class="container-fluid main-container">
		<div class="row">
			<div class="col-xs-12"><h1><?php single_cat_title(); ?></h1></div>
			<main role="main" class="col-xs-12 col-md-9 col-lg-9">
				<div class="posts row">
					<?php get_template_part('loop'); ?>
				</div>
				<?php get_template_part('pagination'); ?>
			</main>
			<div class="hidden-xs hidden-sm col-md-3 col-lg-3">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>