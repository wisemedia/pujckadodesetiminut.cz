<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php get_header(); ?>
	<main class="container">
		<?php if(get_categories()) : ?>
		<ul class="categories">
			<li class="cat-item <?php if(is_home()) { echo 'current-cat'; } ?>"><a href="<?php echo get_permalink(get_option('page_for_posts')); ?>">Todo</a></li>
			<?php
			wp_list_categories(array('title_li' => ''));
			?>
		</ul>
		<?php endif; ?>
		<div class="row">
			<div class="col-md-8">
				<?php if ( have_posts() ) : ?>
					<div class="post-wrapper">
						<div class="row">
							<?php get_template_part('loop'); ?>
						</div>
					</div>
				<?php the_posts_pagination( array(
						'prev_text' => '<span class="screen-reader-text">&lt;</span>',
						'next_text' => '<span class="screen-reader-text">&gt;</span>',
						'screen_reader_text' => ' '
					) );
					endif;
				?>
			</div>
			<div class="hidden-xs hidden-sm col-md-4">
				<div class="box-posts">
					<h3>Lo más leído</h3>
					<ul>
						<?php 
							$popularpost = new WP_Query( array( 'posts_per_page' => 2, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
						?>
						<?php if($popularpost->have_posts()) : ?>
							<?php while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>
								<li>
									<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID, 'latest-post'); ?></a>
									<h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
									<p><?php the_excerpt(); ?></p>
									<a class="read-more" href="<?php the_permalink(); ?>">Sigue leyendo</a>
								</li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</main>

<?php get_footer();
