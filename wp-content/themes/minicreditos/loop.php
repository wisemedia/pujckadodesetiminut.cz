<?php if (have_posts()): while (have_posts()) :
	the_post(); ?>

	<div class="col-md-6">
		<a href="<?php the_permalink(); ?>" class="box-post">
			<div class="gfx" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'post-list'); ?>);">
				<h2><?php the_title(); ?></h2>
			</div>
			<?php the_excerpt(); ?>
			<span class="read-more" href="<?php the_permalink(); ?>">Sigue leyendo</span>
		</a>
	</div>
<?php endwhile; ?>

<?php else: ?>
	<div class="col-xs-12 text-center">
		<h2><?php _e('No se encontraron entradas', 'holaluz'); ?></h2>
	</div>
<?php endif; ?>
